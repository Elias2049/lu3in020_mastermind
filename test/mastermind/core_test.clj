(ns mastermind.core-test
  (:use midje.sweet)
  (:require [clojure.test :refer :all]
            [mastermind.core :refer :all]
            [solver.utils :refer :all]
            [solver.naif :refer :all]))

;; Les fonctions non testées sont des fonctions réalisant une intéraction avec le joueur (nécessitant un read-line)

;; Tests des fonctions de utils.clj contenant les utilitaires nécessaires

(fact "Le `code-secret` est bien composé de couleurs."
      (every? #{:rouge :bleu :vert :jaune :noir :blanc}
              (code-secret 4))
      => true)

(fact "Le `code-secret` a l'air aléatoire."
      (> (count (filter true? (map not=
                                   (repeatedly 20 #(code-secret 4))
                                   (repeatedly 20 #(code-secret 4)))))
         0)
      => true)

(fact "`indications` sont les bonnes."
      (indications [:rouge :rouge :vert :bleu]
                   [:vert :rouge :bleu :jaune])
      => [:color :good :color :bad]

      (indications [:rouge :rouge :vert :bleu]
                   [:bleu :rouge :vert :jaune])
      => [:color :good :good :bad]

      (indications [:rouge :rouge :vert :bleu]
                   [:rouge :rouge :vert :bleu])
      => [:good :good :good :good]

      (indications [:rouge :rouge :vert :vert]
                   [:vert :bleu :rouge :jaune])
      => [:color :bad :color :bad])
;; => true

(fact "les `frequences` suivantes sont correctes."
      (frequences [:rouge :rouge :vert :bleu :vert :rouge])
      => {:rouge 3 :vert 2 :bleu 1}

      (frequences [:rouge :vert :bleu])
      => {:rouge 1 :vert 1 :bleu 1}

      (frequences [1 2 3 2 1 4]) => {1 2, 2 2, 3 1, 4 1})

(fact "Les fréquences disponibles de `freqs-dispo` sont correctes."
      (freqs-dispo [:rouge :rouge :bleu :vert :rouge]
                   [:good :color :bad :good :color])
      => {:bleu 1, :rouge 2, :vert 0})

(fact "Le `filtre-indications` fonctionne bien."
      (filtre-indications [:rouge :rouge :vert :bleu]
                          [:vert :rouge :bleu :jaune]
                          [:color :good :color :bad])
      => [:color :good :color :bad]

      (filtre-indications [:rouge :vert :rouge :bleu]
                          [:rouge :rouge :bleu :rouge]
                          [:good :color :color :color])
      => [:good :color :color :bad])

(println "Test de Test-BonneCombi par midje")

(fact "test de la fonction Test-BonneCombi"
      (Test-BonneCombi [:rouge :vert :noir :blanc] [:rouge :vert :noir :blanc]) => true
      (Test-BonneCombi [:rouge :jaune :noir :blanc] [:rouge :vert :noir :blanc]) => false
      (Test-BonneCombi [:noir :vert :noir :vert] [:rouge :vert :noir :blanc]) => false
      (Test-BonneCombi [:rouge :rouge :rouge :rouge :vert] [:rouge :rouge :rouge :rouge :vert]) => true)

;; Tests des fonctions de naif.clj

(fact "Le `choix-init` est bien composé de couleurs."
      (every? #{:rouge :bleu :vert :jaune :noir :blanc}
              (choix-init 14))
      => true)

(fact "Le `choix-init` a l'air aléatoire."
      (> (count (filter true? (map not=
                                   (repeatedly 20 #(choix-init 15))
                                   (repeatedly 20 #(choix-init 18)))))
         0)
      => true)