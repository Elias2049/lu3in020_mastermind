# Mastermind

![Mastermind](/Mastermind.jpg)

Projet d'implémentation du jeu de Mastermind inventé par Mordecai Meirowitz en 1970. Le jeu se joue dans sa forme classique avec $`4`$ pions pouvant être de $`6`$ couleurs différentes ce qui donne un nombre total de possibilités de :
 $` 6^{4}=1296 `$


## Installation

Pour faire fonctionner l'application, il suffit de faire un `lein run` dans le dossier source et de suivre les indications du terminal.

## Usage

Le jeu est composé de deux modes possibles :

- Le mode CodeBreaker, dans lequel le joeur choisit le nombre `n` de pions avec lequel il veut jouer et essaie de deviner le code tiré aléatoirement par l'ordinateur en effectuant des essais auxquels le programme fera correspondre des indications pour aider le joueur à affiner ses propositions. Les indications données prennent en compte la cardinalité du code et signalent donc en `:bad` tout pion redondant en terme de couleur.
- Le mode Master, dans lequel le joueur pense à un code de taille `n` et répond aux propositions du programme par les indications `:good` pour un pion de la bonne couleur et à la bonne place, `:bad` pour un pion dont la couleur ne fait pas partie du code et `:color` pour un pion dont la couleur fait partie du code mais n'est pas à la bonne place. Dans le mode Master, la cardinalité des propositions n'est pas prise en compte et le joueur ne doit pas spécifier au programme les pions dont la couleur est bonne mais la cardinalité mauvaise. Il mettra donc en `:color` tout pion dont la couleur fait partie du code et ne mettra `:bad` uniquement aux pions dont la couleur ne fait pas partie de son code. Évidemment, il faudra mettre `:bad` dans le cas où le programme a déjà trouvé toutes les places de cette couleur, i.e. ne pas mettre `:color` si la couleur n'a plus à être cherché. 

## Options

 ADD : Nous avons ajouté la possibilité de jouer avec un nombre de pions variable (la version classique du mastermind comporte 4 pions de 6 couleurs différentes).

## Examples

####  Screenshots d'exemples d'execution

![exampleM](/exampleM.png)

![exampleCB](/exampleCB.png)

## Bugs 

Le programme fonctionne de manière correcte pour les deux modes de jeu et le seul blocage dans le mode Master serait lié à des indications erronées de la part du joueur qui se retrouvera avec des `nil` dans les propositions que lui fera le programme. Dans ce cas, il faut quiter le jeu par `Ctrl+c` .

Pour le mode CodeBreaker, le joueur continue à faire des propositions jusqu'à ce que la bonne combinaison soit trouvée. Nous pouvons améliorer cela en imposant un nombre maximal de tentatives pour le joueur qui rendrait le jeu plus intéressant sur un grand nombre de pions (15 ou 20 par exemple que nous avons testé.)

## Tests 

Les  tests sont contenus dans le fichier `mastermind/test/mastermind/core_test.clj` et sont réalisés à l'aide de midje. Il suffit d'éxecuter la commande `lein midje` au niveau du répertoire du projet pour les réaliser. Seules les fonctions ne réalisant pas d'intéractions avec le joueur ont été testées. Celles comportant un `read-line` seraient longues car il faudrait que le testeur tape les propositions sans savoir précisement de quoi il s'agit.

## License

Copyright © 2020 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
