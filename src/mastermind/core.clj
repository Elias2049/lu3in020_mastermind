(ns mastermind.core
  (:gen-class)
  (:use midje.sweet)
  (:require [clojure.set :as set]
            [solver.utils :as uti]
            [solver.naif :as sn]))


(declare lancer-jeu-Master)
(declare lancer-jeu-CB)
(declare lancer-jeu-MM)


(defn -main
  "Lancement du Jeu Mastermind"
  [& args]
  (println "======================== MASTERMIND ========================")
  (println "Welcome to the Clojure version of the Mastermind game as created by Mordecai Meirowitz in 1970")
  (println "====> Would you like to Start playing ?")
  (println "==> 1 : Yes, let's break some code !!")
  (println "==> 2 : No , I have other cats to whipe ('j'ai d'autres chats à fouetter' ... french is weird)")
  (let [command (read-line)]
    (if (= command "2")
      (println "Oh okay , .... I hope that you are kidding about those cats huh? ...")
      (do (println "====> Would you like to be the MASTER of the game or the Code Breaker ?\n==> 1 : Yes, I love Power and I want to be the Master\n==> 2 : No, let's think. I want to be the Code Breaker")
          (let [cmd (read-line)]
            (cond
              (= cmd "1") (do (println "==========Launching Mastermind --- Master mode =========")
                              (println "====> MASTER MODE")
                              (lancer-jeu-MM))
              (= cmd "2") (do (println "==========Launching Mastermind ----Code Breaker mode ==========")
                              (do (println "====> CODE BREAKER MODE\n==> Veuillez choisir le nombre de pions :")
                                  (let [n (Integer/parseInt (read-line))]
                                    (lancer-jeu-CB n))))
              :else (println "Error")))))))


(defn lancer-jeu-MM
  "Lance le Mastermind en mode Solver. L'utilisateur joue contre l'ordinateur qui essaye de découvrir son code"
  []
  (do (println "====> Veuillez saisir le nombre de pions avec lequel vous voulez jouer")
      (let [n (Integer/parseInt (read-line))]
        (println "\n===================> Essai : 0")
        (sn/solver-naif n))))

;; Limiter le nombre de tentatives  // Retirer l'affichage du code choisi par la machine ---> Fait OK
(defn lancer-jeu-CB
  "Lance le Mastermind en mode Code Breaker. L'utilisateur essaye de trouver le code généré aléatoirement par l'ordinateur en faisant une ou plusieurs tentatives"
  [n]
  (let [code-alea (uti/code-secret n)]
    (println "====> L'ordinateur a choisi un code aléatoire :\n==> Faites votre proposition de" n "pions dont les couleurs peuvent être choisies parmi: (rouge bleu vert jaune noir blanc)\n==> Code choisi :" (comment code-alea))
    (loop [] (let [tentative (uti/get-tentative-joueur n)]
               (println " ")
               (println tentative)
               (if (uti/Test-BonneCombi tentative code-alea)
                 (println "\t\t========> BRAVO !!!!! You found the code !!! <========")
                 (do (println "============> Try Again <==========")
                     (recur)))))))


