(ns solver.naif
  (:require [clojure.set :as set]
            [solver.utils :as uti]))


(declare choix-init)
(declare indices-joueur-machine)
(declare deductions)
(declare propose-new)
(declare solver-naif)

;; Contient les fonctions utiles au solveur naïf et permettant de faire des propositions affinées par des déductions données par le joueur

(defn choix-init
  "Construit un vecteur en choisissant de manière aléatoire une série de n pions"
  [n]
  (loop [res []
         ite 0]
    (if (< ite n)
      (recur (conj res (rand-nth (seq uti/Couleurs))) (inc ite))
      res)))

(defn indices-joueur-machine
  "Récupère les indices du joueur étant donné une proposition `prop`. Cette fonction est utilisée dans le mode Master"
  [prop]
  (loop [res []
         ite prop]
    (if (seq ite)
      (do (println "Veuillez évaluer la proposition" prop "\nle pion" (first ite) "est-il bon? Veuillez saisir (good bad ou color) ")
          (let [tmp (keyword (read-line))]
            (if (contains? uti/indics-poss tmp)
              (recur (conj res tmp) (rest ite))
              (do (println "====> Veuillez saisir une indication correcte (good,bad ou color)")
                  (recur res ite)))))
      res)))

(defn deductions
  "Applique les déductions possibles en fonction des indices du joueur et ne garde que les choix possibles qui seront proposés au tour suivant. Cette fonction est utilisée dans le mode Master"
  [poss indices tenta]
  (let [keyss
        (vec (for [x (range (count tenta))]
               (conj (keyword (str x)))))]
    (loop [itera poss
           tests indices
           ite 0]
      (if (seq tests)
        (if (= (first tests) :good)
          (recur (assoc-in itera [(keyword (str ite))] (set (list (get tenta ite)))) (rest tests) (inc ite))
          (if (= (first tests) :color)
            (recur (assoc-in itera [(keyword (str ite))] (set/difference (get itera (keyword (str ite))) (set (list (get tenta ite))))) (rest tests) (inc ite))
            (recur (loop [tmp itera, kk keyss]
                     (if (seq kk)
                       (recur (if (not= (count (get tmp (first kk))) 1)
                                (assoc-in tmp [(first kk)] (set/difference (get tmp (first kk)) (set (list (get tenta ite)))))
                                tmp) (rest kk))
                       tmp)) (rest tests) (inc ite))))
        itera))))

(defn propose-new
  "Retourne un vecteur de n pions de couleurs aléatoires, tirées parmi les couleurs possibles et déterminées à l'aide de la fonction déduction. Cette fonction est utilsée dans le mode Master"
  [prop]
  (loop [p prop
         res []]
    (if (seq p)
      (recur (rest p) (conj res (rand-nth (seq (second (first p))))))
      res)))

(defn solver-naif
  "Implémentation du solveur naif qui tourne jusqu'a obtention du code de l'utilisateur."
  [n]
  (loop [res (zipmap (map keyword (map str (take n (range)))) (take n (repeat uti/Couleurs)))
         tenta (propose-new res)
         indices-j (indices-joueur-machine tenta)
         ite 1]
    (if (or (some #(= :color %) indices-j) (some #(= :bad %) indices-j))
      (do (println "\n===================> Essai :" ite)
          (let [res (deductions res indices-j tenta)
                tenta (propose-new res)
                indices-j (indices-joueur-machine tenta)]
            (recur res tenta indices-j (inc ite))))
      (do (println "voici votre code" tenta)
          (println "\t\t==========>>>>>>>CODE TROUVE<<<<<<<=========")))))
