(ns solver.utils
  (:require [clojure.math.combinatorics :as combin]
            [clojure.set :as set]
            [clojure.string :as str]))


(declare code-secret)
(declare indications)
(declare frequences)
(declare freqs-dispo)
(declare filtre-indications)
(declare Test-BonneCombi)
(declare get-tentative-joueur)

;; Structures de données utiles telles que les couleurs des pions ainsi que les états possibles (:good :color :bad)

(def Couleurs #{:rouge :bleu :vert :jaune :noir :blanc})
(def CouleursAssociation {0 :rouge 1 :bleu 2 :vert 3 :jaune 4 :noir 5 :blanc})
(def indics-poss #{:good :color :bad})

(defn code-secret
  "Tirage aleatoire de n pions pour former un code secret"
  [n]
  (let [coul [:rouge :bleu :vert :jaune :noir :blanc]]
    (loop [ite 0
           res []]
      (if (>= ite n)
        res
        (recur (inc ite) (conj res (rand-nth coul)))))))

(defn indications
  "Retourne les indications du mastermind en fonction des tentatives proposees"
  [c t]
  (let [size (count t)]
    (loop [ite 0
           res []]
      (if (>= ite size)
        res
        (recur (inc ite) (if (= (nth c ite) (nth t ite))
                           (conj res :good)
                           (if (some #(= (nth t ite) %) c)
                             (conj res :color)
                             (conj res :bad))))))))

(defn frequences
  "Retourne la map des frequences des couleurs dans un code ou une tentative"
  [c]
  (let [size (count c)]
    (loop [c1 c
           ite 0
           res {}]
      (if (>= ite size)
        res
        (recur c1 (inc ite) (assoc res (nth c ite) (loop [tmp c1
                                                          ite1 0
                                                          cpt 0]
                                                     (if (>= ite1 size)
                                                       cpt
                                                       (if (= (nth tmp ite) (nth tmp ite1))
                                                         (recur tmp (inc ite1) (inc cpt))
                                                         (recur tmp (inc ite1) cpt))))))))))

(defn freqs-dispo
  "Retourne la map de frequences a partir d'un code secret et d'un vecteur indications"
  [c v]
  (let [size (count c)]
    (loop [tmp (frequences c)
           res {}]
      (if (seq tmp)
        (recur (rest tmp) (assoc res (ffirst tmp) (loop [ite 0
                                                         cpt (second (first tmp))]
                                                    (if (>= ite size)
                                                      cpt
                                                      (recur (inc ite) (if (= (ffirst tmp) (nth c ite))
                                                                         (if (= (nth v ite) :good)
                                                                           (dec cpt)
                                                                           cpt)
                                                                         cpt))))))
        res))))

(defn filtre-indications
  "Retourne la bonne indication en tenant compte de la cardinalité de chaque couleur"
  [c t ind]
  (loop [tmp (freqs-dispo c ind)
         res ind]
    (if (seq tmp)
      (recur (rest tmp) (let [card (second (first tmp))]
                          (loop [acc 0
                                 res res
                                 ite 0]
                            (if (>= ite (count c))
                              res
                              (if (= (ffirst tmp) (nth t ite))
                                (if (= (nth ind ite) :color)
                                  (if (>= acc card)
                                    (recur (inc acc) (assoc res ite :bad) (inc ite))
                                    (recur (inc acc) res (inc ite)))
                                  (recur acc res (inc ite)))
                                (recur acc res (inc ite)))))))
      res)))

;; Fonctions utiles aux intéractions entre le joueur et le jeu dont les affichages ont lieu sur terminal

(defn Test-BonneCombi
  "Verifie la justesse de la combinaison proposée par le joueur. Renvoie True si le bon code a été trouvé. Cette fonction est utilisée dans le mode CodeBreaker"
  [tentative code]
  (let [indic (indications code tentative)
        etat (filtre-indications code tentative indic)]
    (println "====> Vos pions sont placés tel que :\n====> " etat)
    (loop [s etat]
      (if (seq s)
        (if (not= (first s) :good)
          false
          (recur (rest s)))
        true))))

(defn get-tentative-joueur
  "Récupère les `n` couleurs saisies par le joueur sur la ligne de commande. Si une couleur non reconnue est utilisée, l'utilisateur doit retenter jusqu'à obtenir une tentative correcte. Cette fonction est utilisée dans le mode"
  [n]
  (loop [ite 1
         tentative []]
    (if (<= ite n)
      (do (println "====> Choisir la couleur" ite)
          (let [tmp (keyword (read-line))]
            (if (contains? Couleurs tmp)
              (recur (inc ite) (conj tentative tmp))
              (do (println "====> !!!! Mauvaise tentative, Veuillez choisir une couleur parmi (rouge bleu vert jaune noir blanc) !!!!")
                  (recur ite tentative)))))
      tentative)))

